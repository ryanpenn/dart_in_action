///
/// loop
///
main(List<String> args) {
  // for loop
  for (int i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      print(i);
    }
  }

  print('');

  // for ..in loop
  List planetList = ["Mercury", "Venus", "Earth", "Mars"];
  for (String planet in planetList) {
    print(planet);
  }

  print('');

  // while loop
  var i = 1;
  while (i <= 10) {
    if (i % 2 == 0) {
      print(i);
    }
    i++;
  }

  print('');

  // do ..while loop
  do {
    i--;
    if (i < 5) {
      print(i);
    }
  } while (i > 0);

  print('');

  // break labels
  breakLabel: for (int x = 1; x < 100; x++) {
    for (int y = 1; y < 10; y++) {
      print("x=$x, y=$y");
      if (x == 1 && y == 3) {
        break breakLabel;
      }
    }
  }

  print('');

  // continue labels
  continueLabel: for (var m = 1; m < 3; m++) {
    for (var n = 0; n < 10; n++) {
      print("m=$m, n=$n");
      if (n==2) {
        continue continueLabel; 
      }
    }
  }
}
