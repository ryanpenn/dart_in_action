/**
外观模式（Facade Pattern）

意图：为子系统中的一组接口提供一个一致的界面，外观模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。
主要解决：降低访问复杂系统的内部子系统时的复杂度，简化客户端与之的接口。
何时使用： 
  1、客户端不需要知道系统内部的复杂联系，整个系统只需提供一个"接待员"即可。
  2、定义系统的入口。
如何解决：客户端不与系统耦合，外观类与系统耦合。
*/
main(List<String> args) {
  ShapeMaker shapeMaker = new ShapeMaker();
  shapeMaker.drawCircle();
  shapeMaker.drawRectangle();
  shapeMaker.drawSquare();
}

//////////////////////////////////////////////////////////////////

///
/// 创建一个接口
///
abstract class Shape {
  void draw();
}

///
/// 创建实现接口的实体类
///
class Rectangle implements Shape {
  @override
  void draw() {
    print("Rectangle::draw()");
  }
}

class Square implements Shape {
  @override
  void draw() {
    print("Square::draw()");
  }
}

class Circle implements Shape {
  @override
  void draw() {
    print("Circle::draw()");
  }
}

///
/// 创建一个外观类
///
class ShapeMaker {
  Shape circle;
  Shape rectangle;
  Shape square;

  ShapeMaker() {
    circle = Circle();
    rectangle = Rectangle();
    square = Square();
  }

  void drawCircle() {
    circle.draw();
  }

  void drawRectangle() {
    rectangle.draw();
  }

  void drawSquare() {
    square.draw();
  }
}
