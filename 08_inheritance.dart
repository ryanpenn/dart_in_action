///
/// inheritance.dart
///
main(List<String> args) {
  Animal animal = Dog('black');
  animal.eat();
  (animal as Dog).bark();

  if (animal is Duck) {
    print('Yes');
  } else {
    print('No');
  }

  Duck duck = Duck('white');
  duck.eat();
  print(duck is Animal);

  YellowFlyDuck yellowFlyDuck = YellowFlyDuck();
  yellowFlyDuck.eat();
  yellowFlyDuck.flyInSky();
  yellowFlyDuck.count();
  yellowFlyDuck.output();

  print(Duck.type);
  // print(YellowFlyDuck.type);
}

class Animal {
  String color;
  eat() {
    print('Eat!');
  }
}

class Dog extends Animal {
  Dog(String color) {
    super.color = color;
  }

  @override
  eat() {
    print('$color dog eats meat.');
  }

  void bark() {
    print("Bark !");
  }
}

class Duck extends Animal {
  static String type = "DUCK";

  Duck(String color) {
    super.color = color;
  }

  @override
  eat() {
    print('$color duck eats rice.');
  }
}

abstract class Fly {
  void flyInSky();
}

class CountableMixin {
  int _count = 0;

  void count() {
    _count++;
  }

  output() {
    print('count: $_count');
  }
}

class YellowFlyDuck extends Duck with CountableMixin implements Fly {
  YellowFlyDuck() : super('Yellow');

  @override
  void flyInSky() {
    print('$color duck Fly!');
  }
}
