/**
备忘录模式（Memento Pattern）

意图：在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态。
主要解决：所谓备忘录模式就是在不破坏封装的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态，这样可以在以后将对象恢复到原先保存的状态。
何时使用：很多时候我们总是需要记录一个对象的内部状态，这样做的目的就是为了允许用户取消不确定或者错误的操作，能够恢复到他原先的状态，使得他有"后悔药"可吃。
如何解决：通过一个备忘录类专门存储对象状态。
*/
main(List<String> args) {
  Originator originator = Originator();
  CareTaker careTaker = CareTaker();
  originator.setState("State #1");
  originator.setState("State #2");
  careTaker.add(originator.saveStateToMemento());

  originator.setState("State #3");
  careTaker.add(originator.saveStateToMemento());

  originator.setState("State #4");
  print("Current State: ${originator.getState()}");

  originator.getStateFromMemento(careTaker.get(0));
  print("First saved State: ${originator.getState()}");

  originator.getStateFromMemento(careTaker.get(1));
  print("Second saved State: ${originator.getState()}");
}

//////////////////////////////////////////////////////////////////

///
/// 创建 Memento 类
///
class Memento {
  String _state;

  Memento(this._state);

  String getState() {
    return _state;
  }
}

///
/// 创建 Originator 类
///
class Originator {
  String _state;

  void setState(String state) {
    this._state = state;
  }

  String getState() {
    return _state;
  }

  Memento saveStateToMemento() {
    return Memento(_state);
  }

  void getStateFromMemento(Memento memento) {
    _state = memento.getState();
  }
}

///
/// 创建 CareTaker 类
///
class CareTaker {
  List<Memento> _mementoList = List<Memento>();

  void add(Memento state) {
    _mementoList.add(state);
  }

  Memento get(int index) {
    return _mementoList[index];
  }
}
